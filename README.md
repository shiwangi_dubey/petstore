# PetStore

PetStore is an API Automation Test Suite. Some APIs of end point **pet** and some of **store** are automated. Workflows of place order for **'Available'** pet and **'Newly Added Pet'** are also automated

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* JRE , JDK
* Any IDE.   
> example - Eclipse
* Maven

### Installation
* Go to the [GitLab homepage](https://gitlab.com/). At the top, search for **PetStore.**


* Select PetStore, it should be in the listed results as **shiwangi dubey / PetStore**.

* Click on Clone dropdown.


* Copy the **“Clone with HTTPS”** link using the clipboard icon at the bottom right of the page’s side-bar.

* Open Terminal, use the command git clone, then paste the link from your clipboard, or copy the command and link from below:
  > git clone https://github.com/shiwangi dubey/PetStore.git

* Change directories to the new **~/PetStore** directory:
  > cd ~/PetStore/
* To ensure that your master branch is up-to-date, use the pull command:
  > git pull https://github.com/shiwangi dubey/PetStore.git master


## Running the tests


#### Use Runner Class of every package to Execute the Test Suit.
* Run Without Tags - will execute all feature files
  > public class StoreRunner {
      @Test
    public void testParallel() {
    Results results = Runner.parallel(getClass(), 0, "target/surefire-reports");
    assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
	}
* Run With Tags - will execute only tagged feature files
   > @KarateOptions(tags = {"~@ignore" ,"@PetStore"})
   public class StoreRunner {
    @Test
    public void testParallel() {
    Results results = Runner.parallel(getClass(), 0, "target/surefire-reports");
    assertTrue(results.getErrorMessages(), results.getFailCount() == 0);

#### Use Command line to Execute
* Run Without Tags - will execute all feature files
  > mvn test -Dtest=PetRunner
* Run With Tags - will execute only tagged feature files
  > mvn test -Dkarate.options="--tags @PetStore"

## Built With
* [Maven](https://maven.apache.org/) - Build Automation Tool / Dependency Management
* [Cucumber](https://cucumber.io/docs/installation/java/) - Supports BDD FrameWork
* [Karate-DSL](https://github.com/intuit/karate) - Open Source Automation Tool

## Contributing
Pull requests are welcome. Please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

Please make sure to update tests as appropriate.

