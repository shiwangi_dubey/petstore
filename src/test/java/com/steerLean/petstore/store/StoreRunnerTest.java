package com.steerLean.petstore.store;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import com.intuit.karate.KarateOptions;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;

@KarateOptions(tags = {"~@ignore"})
public class StoreRunnerTest{

	@Test
	public void testParallel() {
		Results results = Runner.parallel(getClass(), 0, "target/surefire-reports");
		assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
	}

}
