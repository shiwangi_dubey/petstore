@ignore
Feature: Store Core

  Background: Set URL
    * url storeUrl

  # WorkFlow Test For New Pet
  @PlaceOrder
  Scenario: Place order for a pet
    * def req = {id:'#(id)',qty:'#(qty)'}
    * def id = req.id
    * def quantity = req.qty
    * print '***********Place Order**************'
    When path 'order'
    When request { id:'#(id)', petId:'#(id)', quantity:'#(quantity)', shipDate:'2021-01-01T16:00:30.083Z', status:'placed', complete: true }
    And method post
    Then status 200
    * match response.id == id
    * match response.petId == id
    * match response == { id:'#notnull', petId:'#notnull', quantity:'#notnull', shipDate:'#notnull', status:'#notnull', complete: '#notnull' }
