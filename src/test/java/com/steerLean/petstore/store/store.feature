@PetStore @Store 
Feature: Execute Store Test Cases 

Background: Set URL 
	* url storeUrl

	# WorkFlow Test For Available Pet
@PlaceOrder 
Scenario Outline: Place order for an available pet 
	* def fetchByStatus = call read('classpath:com/steerlean/petstore/pet/pet-core.feature@FindByStatusCore') {status:'available'}
    * def id = fetchByStatus.response[0].id
    * def placeOrder = call read('store-core.feature@PlaceOrder') {id:'#(id)',qty:<qty>}

	@Positive 
	Examples: 
	| qty |
	|   1 |
	|   2 |
	| 100 |
	
	@Negative 
	Examples: 
	| qty       |
	|         0 |
	|      2.25 |
	| 100000000 |
	
	# WorkFlow Test For New Pet
	@PlaceOrder 
	Scenario Outline: Place order for a pet 
	* def addNewPet = call read('classpath:com/steerlean/petstore/pet/pet-core.feature@AddNewPetCore') {status:<status>}
	* def id = addNewPet.response.id 
	* def placeOrder = call read('store-core.feature@PlaceOrder') {id:'#(id)',qty:'2'}
	
	@Positive 
	Examples: 
	| status      | qty |
	| 'available' |   1 |
	| 'available' |   2 |
	| 'available' | 100 |
	
	@Negative 
	Examples: 
	| status    | qty       |
	| 'sold'    |         0 |
	| 'pending' |      2.25 |
	| 'pending' | 100000000 |
	
	@FindPurchaseById 
	Scenario: Fetch purchase by Id 
	* print '***********Fetch Order**************' 
	* def addNewPet = call read('classpath:com/steerlean/petstore/pet/pet-core.feature@AddNewPetCore') {status:'available'}
	* def id = addNewPet.response.id 
	* def placeOrder = call read('store-core.feature@PlaceOrder') {id:'#(id)',qty:'2'}
	* def orderId = placeOrder.response.id 
	When   path 'order' , orderId 
	And   method get 
	Then   status 200 
	* match response == { id:'#notnull', petId:'#notnull', quantity:'#notnull',shipDate:'#notnull', status:'#notnull', complete: '#notnull' }
	
	@DeleteOrderById 
	Scenario: Delete Order By Id 
	* print '***********Delete Order**************' 
	* def addNewPet = call read('classpath:com/steerlean/petstore/pet/pet-core.feature@AddNewPetCore') {status:'available'}
	* def id = addNewPet.response.id 
	* def placeOrder = call read('store-core.feature@PlaceOrder') {id:'#(id)',qty:'2'}
	* def orderId = placeOrder.response.id 
	When   path 'order', orderId 
	And   method delete 
	Then   status 200 
	* match response.message == id.toString() 
