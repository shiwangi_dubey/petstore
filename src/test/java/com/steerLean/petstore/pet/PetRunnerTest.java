package com.steerLean.petstore.pet;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;

@KarateOptions(tags = {"~@ignore"})
public class PetRunnerTest {

	@Test
	public void testParallel() {
		Results results = Runner.parallel(getClass(), 0);
	}

}
