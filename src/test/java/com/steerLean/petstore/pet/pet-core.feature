@ignore
Feature: Pet Core

  Background: Set URL
    * url petUrl

  @AddNewPetCore
  Scenario: Add a New pet to store
    * def req = {status:'#(status)'}
    * print '***********Create New Pet**************'
    * def id = 0
    * def fun =
      """
      function(){
      var randomid = 0
       randomid = Math.floor(Math.random() * 100);
       karate.set('id' , parseInt(randomid.toPrecision()))
      return id;
      }
      """
    * call fun()
    * def category = { id: 01, name: "Lebra"}
    * def tags = [{ id: 01, name: "White"}]
    * def photoUrls = ["http://whiteDog1.com/","http://whiteDog2.com/"]
    When request {id: '#(id)', name: 'Rocky', status: '#(status)',category: '#(category)',photoUrls: '#(photoUrls)',tags: '#(tags)'}
    And method post
    Then status 200
    * match response.id == id
    * match response == {id: '#notnull', name: '#notnull', status: '#notnull',category: '#notnull',photoUrls: '#notnull',tags: '#notnull'}

  @FindByStatusCore
  Scenario: Fetch pets by status
    * def req = {status:'#(status)'}
    * def reqstatus = req.status
    * print '***********Fetch Pet By Status**************'
    When path 'findByStatus'
    * param status = reqstatus
    And method get
    Then status 200
