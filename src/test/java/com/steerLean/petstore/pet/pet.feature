@PetStore @Pet 
Feature: Execute Pet Test Cases 

Background: Set URL 
	* url petUrl

@AddNewPet 
Scenario: Add a New pet to store 
	* def addNewPet = call read('pet-core.feature@AddNewPetCore') {status:'available'}

	#Updating Name to Lovy
@UpdateExistingPet 
Scenario Outline: Update an Existing pet 
	* print '*********** Update Existing Pet**************'
    * def addNewPet = call read('pet-core.feature@AddNewPetCore') {status:'available'}
    * def id = addNewPet.response.id
    * def category = { id: 01, name: "Lebra"}
    * def tags = [{ id: 01, name: "White"}]
    * def photoUrls = ["http://whiteDog1.com/","http://whiteDog2.com/"]
	When request {id: '#(id)', name: <updatedName>, status: 'available',category: '#(category)',photoUrls: '#(photoUrls)',tags: '#(tags)'} 
	And method put 
	Then status 200 
	* match response.name == <updatedName> 
		* match response == {id: '#notnull', name: '#notnull', status: '#notnull',category: '#notnull',photoUrls: '#notnull',tags: '#notnull'}

		Examples: 
		| updatedName |
		| 'Lovy'      |
		
@FindByStatus 
Scenario Outline: Fetch pets by status 
	* def findByStatus = call read('pet-core.feature@FindByStatusCore') { status: <status> }

	@PositiveCases 
	Examples: 
	| status      |
	| 'available' |
	| 'pending'   |
	| 'sold'      |
	
	@NagativeCases 
	Examples: 
	| status |
	| ''     |
	| 'abc'  |
	| null   |
	
	@UpdateWithFormData 
	Scenario: Update pet with form data 
	* print '***********Update Pet With FormData**************' 
	* def addNewPet = call read('pet-core.feature@AddNewPetCore') { status:'available' }
	* def id = addNewPet.response.id 
	When  path id 
	And  header Content-Type = 'application/x-www-form-urlencoded' 
	And  def formFields = { name: 'chanchung', status: 'available' } 
	And  form fields formFields 
	And  method post 
	Then  status 200 
	* match response.message == id.toString() 
	
	@DeletePet 
	Scenario: Delete pet 
	* print '***********Delete Pet **************' 
	* def addNewPet = call read('pet-core.feature@AddNewPetCore') { status:'available' }
	* def id = addNewPet.response.id 
	When  path id 
	And  method delete 
	Then  status 200 
	* match response.message == id.toString() 
